<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * FIle name   : index.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

use App\Kernel;

/**
 * This application's name.
 */
const APP_NAME = 'GitManager';

/**
 * This application's root directory.
 */
const ROOT = __DIR__;

require_once ROOT . '/vendor/autoload.php';

session_start();
new Kernel();
