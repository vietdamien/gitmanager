<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Strings
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Emailing;

class Strings
{
    public const BODY    = 'body';
    public const DATE    = 'date';
    public const FILTERS = 'filters';
    public const NAME    = 'name';
    public const VALUE   = 'value';
    public const VERSION = 'version';
    public const V3_1    = 'v3.1';

    public const ADD_FORCE    = 'addforce';
    public const ADD_NO_FORCE = 'addnoforce';
    public const REMOVE       = 'remove';
    public const UNSUBSCRIBE  = 'unsub';

    public const ADDED        = 'added';
    public const AUTHOR_EMAIL = 'author_email';
    public const AUTHOR_NAME  = 'author_name';
    public const COUNT        = 'count';
    public const MODIFIED     = 'modified';
    public const PROJECT_NAME = 'project_name';
    public const MESSAGE      = 'message';
    public const PASSWORD     = 'password';
    public const REMOVED      = 'removed';
    public const URL          = 'url';

    public const C_ACTION           = 'Action';
    public const C_CONTACT_ALT      = 'ContactAlt';
    public const C_CONTACTS_LISTS   = 'ContactsLists';
    public const C_CONTACTS_LIST_ID = 'ContactsListID';
    public const C_DATA             = 'Data';
    public const C_DATATYPE         = 'Datatype';
    public const C_EMAIL            = 'Email';
    public const C_LIMIT            = 'Limit';
    public const C_LIST_ID          = 'ListID';
    public const C_LOCALE           = 'Locale';
    public const C_NAME             = 'Name';
    public const C_NAME_SPACE       = 'NameSpace';

    public const C_FROM              = 'From';
    public const C_SUBJECT           = 'Subject';
    public const C_TITLE             = 'Title';
    public const C_TO                = 'To';
    public const C_HTML_PART         = 'HTMLPart';
    public const C_MESSAGES          = 'Messages';
    public const C_ME                = 'Me';
    public const C_TEXT_PART         = 'TextPart';
    public const C_SENDER            = 'Sender';
    public const C_SENDER_EMAIL      = 'SenderEmail';
    public const C_SENDER_NAME       = 'SenderName';
    public const C_TEMPLATE_ID       = 'TemplateID';
    public const C_TEMPLATE_LANGUAGE = 'TemplateLanguage';
    public const C_VARIABLES         = 'Variables';

    public const FIELD_BOOL     = 'bool';
    public const FIELD_DATETIME = 'datetime';
    public const FIELD_FLOAT    = 'float';
    public const FIELD_INT      = 'int';
    public const FIELD_STR      = 'str';

    public const U_ID = 'ID';

    private function __construct()
    {
    }
}
