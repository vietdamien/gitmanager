<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : EmailAPI
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Emailing;

use JetBrains\PhpStorm\ArrayShape;
use Mailjet\Client;
use Mailjet\Resources;

class EmailAPI extends API
{
    private Client $clientV3_1;

    /**
     * EmailAPI constructor.
     * Initializes a client for email sending and a standard one for any other operations.
     *
     * @param string $publicKey  the public key of the API
     * @param string $privateKey the private key of the API
     */
    public function __construct(string $publicKey, string $privateKey)
    {
        parent::__construct($publicKey, $privateKey);
        $this->clientV3_1 = new Client($publicKey, $privateKey, true, [Strings::VERSION => Strings::V3_1]);
    }

    /**
     * Gets the number of sent messages to the given recipient.
     *
     * @param string $recipient the given recipient
     *
     * @return int the number of sent messages to the given recipient
     */
    public function countSent(string $recipient): int
    {
        return sizeof($this->getSent($recipient));
    }

    /**
     * Gets the list of sent messages to the given recipient.
     *
     * @param string $recipient the given recipient
     *
     * @return array the result as a JSON array
     */
    public function getSent(string $recipient): array
    {
        $filters = [
                Strings::C_CONTACT_ALT => $recipient,
        ];

        return $this->getResults(Resources::$Message, [Strings::FILTERS => $filters]);
    }

    /**
     * Sends an email.
     *
     * @param array      $recipients the recipient of the sent email
     * @param string     $subject    the subject of the sent email
     * @param int        $templateId the transactional template email ID
     * @param array      $sender     the sender's information
     * @param array|null $variables  the custom variables to include in the email
     *
     * @return array the result as a JSON array
     */
    public function send(
            array $recipients,
            string $subject,
            int $templateId,
            array $sender,
            array $variables = null
    ): array {
        $body = $this->buildBody($recipients, $subject, $templateId, $sender, $variables);

        $response = $this->clientV3_1->post(Resources::$Email, [Strings::BODY => $body]);
        return $response->getData();
    }

    #[ArrayShape([Strings::C_MESSAGES => "array"])] private function buildBody(
            array $recipients,
            string $subject,
            int $templateId,
            array $sender,
            ?array $variables
    ): array {
        $body = [Strings::C_MESSAGES => []];

        foreach ($recipients as $recipient) {
            $body[Strings::C_MESSAGES][] = [
                    Strings::C_FROM              => $sender/*[
                            Strings::C_EMAIL => Configuration::SENDER_EMAIL,
                            Strings::C_NAME  => Configuration::SENDER_NAME
                    ]*/,
                    Strings::C_TO                => [
                            [
                                    Strings::C_EMAIL => $recipient,
                                    Strings::C_NAME  => Strings::C_ME,
                            ],
                    ],
                    Strings::C_VARIABLES         => $variables,
                    Strings::C_TEMPLATE_ID       => $templateId,
                    Strings::C_TEMPLATE_LANGUAGE => true,
                    Strings::C_SUBJECT           => $subject,
            ];
        }

        return $body;
    }
}
