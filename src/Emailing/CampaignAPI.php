<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : CampaignAPI
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Emailing;

use Mailjet\Resources;

class CampaignAPI extends API
{
    /**
     * CampaignAPI constructor.
     *
     * @param string $publicKey  the public key of the API
     * @param string $privateKey the private key of the API
     */
    public function __construct(string $publicKey, string $privateKey)
    {
        parent::__construct($publicKey, $privateKey);
    }

    /**
     * Completes the body part of the campaign draft that has the given campaign ID.
     *
     * @param int    $campaignId the given campaign ID
     * @param string $html       the HTML part of the campaign
     * @param string $text       the text part of the campaign
     *
     * @return array the result as a JSON array
     */
    public function completeDraftBody(int $campaignId, string $html, string $text): array
    {
        $body = [
                Strings::C_HTML_PART => $html,
                Strings::C_TEXT_PART => $text,
        ];
        return $this->post(
                Resources::$CampaigndraftDetailcontent,
                [Strings::U_ID => $campaignId, Strings::BODY => $body]
        );
    }

    /**
     * Creates a campaign draft with the main information (not including the body, but only the title, sender
     * information...).
     *
     * @param string $title          the title of the created campaign draft
     * @param string $subject        the subject of the created campaign draft
     * @param string $contactsListId the list of contact IDs that are concerned by the created campaign draft
     * @param string $senderEmail    the sender's email address
     * @param string $senderName     the sender's name
     * @param string $locale         the locale of the created campaign draft
     *
     * @return array the result as a JSON array
     */
    public function createDraft(
            string $title,
            string $subject,
            string $contactsListId,
            string $senderEmail,
            string $senderName,
            string $locale = Configuration::DEFAULT_LOCALE
    ): array {
        $body = [
                Strings::C_LOCALE           => $locale,
                Strings::C_SENDER           => $senderName,
                Strings::C_SENDER_NAME      => $senderName,
                Strings::C_SENDER_EMAIL     => $senderEmail,
                Strings::C_SUBJECT          => $subject,
                Strings::C_CONTACTS_LIST_ID => $contactsListId,
                Strings::C_TITLE            => $title,
        ];
        return $this->post(Resources::$Campaigndraft, [Strings::BODY => $body]);
    }

    /**
     * Gets the statistics for the campaign that has the given campaign ID.
     *
     * @param int $campaignId the given campaign ID
     *
     * @return array the result as a JSON array
     */
    public function getStatistics(int $campaignId): array
    {
        return $this->getResults(Resources::$Campaignstatistics, [Strings::U_ID => $campaignId]);
    }

    /**
     * Gets the list of all the campaigns.
     *
     * @return array the result as a JSON array
     */
    public function getList(): array
    {
        return $this->getResults(Resources::$Campaignoverview);
    }

    /**
     * Schedules the campaign that has the given campaign ID at the given date.
     *
     * @param string $campaignID the given campaign ID
     * @param string $date       the given date
     *
     * @return array the result as a JSON array
     */
    public function schedule(string $campaignID, string $date): array
    {
        $body = [
                Strings::DATE => $date,
        ];
        return $this->post(Resources::$CampaigndraftSchedule, [Strings::U_ID => $campaignID, Strings::BODY => $body]);
    }

    /**
     * Sends the campaign that has the given campaign ID.
     *
     * @param int $campaignID the given campaign ID
     *
     * @return array the result as a JSON array
     */
    public function send(int $campaignID)
    {
        return $this->post(Resources::$CampaigndraftSend, [Strings::U_ID => $campaignID]);
    }
}
