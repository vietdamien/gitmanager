<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Configuration
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Emailing;

class Configuration
{
    public const DEFAULT_LOCALE = 'en_US';
    public const RESULTS_LIMIT  = 1000;

    private function __construct()
    {
    }
}
