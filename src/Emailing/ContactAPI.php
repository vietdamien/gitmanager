<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : ContactAPI
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Emailing;

use Closure;
use Mailjet\Resources;

class ContactAPI extends API
{
    /**
     * ContactAPI constructor.
     *
     * @param string $publicKey  the public key of the API
     * @param string $privateKey the private key of the API
     */
    public function __construct(string $publicKey, string $privateKey)
    {
        parent::__construct($publicKey, $privateKey);
    }

    /**
     * Creates a contact.
     *
     * @param string $name  the name of the created contact
     * @param string $email the email address of the created contact
     *
     * @return array the result as a JSON array the result as a JSON array
     */
    public function create(string $name, string $email): array
    {
        $body = [
                Strings::C_NAME  => $name,
                Strings::C_EMAIL => $email,
        ];
        return $this->post(Resources::$Contact, [Strings::BODY => $body]);
    }

    /**
     * Creates a custom contact field for more granular details.
     *
     * @param string $name      the name of the custom field
     * @param string $dataType  the type of the data that can be either 'str', 'int', 'float', 'datetime' or 'bool'
     * @param string $nameSpace the storing mode that can be either 'static' in order to store only one value per
     *                          DataType or 'historic' to keep a timestamped history of the value
     *
     * @return array the result as a JSON array
     */
    public function createField(string $name, string $dataType, string $nameSpace): array
    {
        $possibleTypes = [
                Strings::FIELD_BOOL,
                Strings::FIELD_DATETIME,
                Strings::FIELD_FLOAT,
                Strings::FIELD_INT,
                Strings::FIELD_STR,
        ];

        if (in_array($dataType = strtolower($dataType), $possibleTypes)) {
            $body = [
                    Strings::C_NAME       => $name,
                    Strings::C_DATATYPE   => $dataType,
                    Strings::C_NAME_SPACE => $nameSpace,
            ];
        }

        return isset($body) ? $this->post(Resources::$Contactmetadata, [Strings::BODY => $body]) : [];
    }

    /**
     * Creates a new contacts list.
     *
     * @param string $listName the name of the created contacts list
     *
     * @return array the result as a JSON array
     */
    public function createList(string $listName): array
    {
        $body = [Strings::C_NAME => $listName];
        return $this->post(Resources::$Contactslist, [Strings::BODY => $body]);
    }

    /**
     * Gets the contact that has the given contact ID.
     *
     * @param int $contactId the given contact ID
     *
     * @return array the result as a JSON array
     */
    public function get(int $contactId): array
    {
        return $this->getResults(Resources::$Contact, [Strings::U_ID => $contactId]);
    }

    /**
     * Gets the list of contacts.
     *
     * @return array the result as a JSON array
     */
    public function getAll(): array
    {
        return $this->getResults(
                Resources::$Contact,
                [Strings::FILTERS => [Strings::C_LIMIT => Configuration::RESULTS_LIMIT]]
        );
    }

    /**
     * Gets the fields that define a contact.
     *
     * @return array the result as a JSON array
     */
    public function getFields(): array
    {
        return $this->getResults(Resources::$Contactmetadata);
    }

    /**
     * Gets the list of contact data for all the contacts if the contact ID is not specified.
     *
     * @param string|null $contactId the contact ID
     *
     * @return array the result as a JSON array
     */
    public function getFieldValues(string $contactId = null): array
    {
        return !$contactId
                ? $this->getResults(Resources::$Contactdata)
                : $this->getResults(Resources::$Contactdata, [Strings::U_ID => $contactId]);
    }

    /**
     * Gets all the contacts that are in the list that has the given contacts list ID.
     *
     * @param int $contactsListId the given contacts list ID
     *
     * @return array|Closure the result as a JSON array
     */
    public function getFromList(int $contactsListId): array|Closure
    {
        return array_filter(
                $this->getAll(),
                function ($contact) use (&$contactsListId) {
                    $subscriptionsLists = $this->getSubscriptionLists($contact[Strings::U_ID]);
                    $isSubscribed       = false;

                    foreach ($subscriptionsLists as $list) {
                        $isSubscribed |= $list[Strings::C_LIST_ID] == $contactsListId;
                    }

                    return $subscriptionsLists != null && $isSubscribed;
                }
        );
    }

    /**
     * Gets the list of contacts lists.
     *
     * @return array the result as a JSON array
     */
    public function getLists(): array
    {
        return $this->getResults(Resources::$Contactslist);
    }

    /**
     * Gets the list of contacts lists the contact that has the given contact ID is subscribed to.
     *
     * @param string|null $contactID the given contact ID
     *
     * @return array the result as a JSON array
     */
    public function getSubscriptionLists(string $contactID = null): array
    {
        return $this->getResults(Resources::$ContactGetcontactslists, [Strings::U_ID => $contactID]);
    }

    /**
     * Removes the contact that has the given contact ID from the given contacts lists.
     *
     * @param int   $contactID       the given contact ID
     * @param array $contactsListIDs the given contacts lists
     *
     * @return array the result as a JSON array
     */
    public function removeFromLists(int $contactID, array $contactsListIDs): array
    {
        return $this->editLists(Strings::REMOVE, $contactID, $contactsListIDs);
    }

    /**
     * Subscribes the contact that has the given contact ID to the given contacts lists.
     *
     * @param int   $contactID       the given contact ID
     * @param array $contactsListIDs the given contacts lists
     *
     * @return array the result as a JSON array
     */
    public function subscribeToLists(int $contactID, array $contactsListIDs): array
    {
        return $this->editLists(Strings::ADD_FORCE, $contactID, $contactsListIDs);
    }

    /**
     * Un-subscribes the contact that has the given contact ID from the given contacts lists.
     *
     * @param int   $contactID       the given contact ID
     * @param array $contactsListIDs the given contacts lists
     *
     * @return array the result as a JSON array
     */
    public function unsubscribeFromLists(int $contactID, array $contactsListIDs): array
    {
        return $this->editLists(Strings::UNSUBSCRIBE, $contactID, $contactsListIDs);
    }

    /**
     * Updates the given fields for the contact that has the given contact ID.
     *
     * @param string $contactID the given contact ID
     * @param array  $fields    the given fields as key-value pairs
     *
     * @return array the result as a JSON array
     */
    public function updateFields(string $contactID, array $fields): array
    {
        $body = [Strings::C_DATA => []];
        foreach ($fields as $key => $value) {
            $body[Strings::C_DATA][] = [Strings::C_NAME => $key, Strings::VALUE => $value];
        }
        return $this->put(Resources::$Contactdata, [Strings::U_ID => $contactID, Strings::BODY => $body]);
    }

    /**
     * Updates the contacts list that has the given contacts list ID from the given updated contacts list.
     *
     * @param int   $contactsListID      the given contacts list ID
     * @param array $updatedContactsList the given updated contacts list
     */
    public function updateLists(int $contactsListID, array $updatedContactsList)
    {
        // Start by un-subscribing people who are not in the updated list anymore
        $unsubscribe = array_diff_key($old = $this->getFromList($contactsListID), $updatedContactsList);
        foreach ($unsubscribe as $item) {
            $this->removeFromLists($item[Strings::U_ID], [$contactsListID]);
        }

        // Then subscribe new subscribers
        $subscribe = array_diff_key($updatedContactsList, $old);
        foreach ($subscribe as $item) {
            $this->subscribeToLists($item[Strings::U_ID], [$contactsListID]);
        }
    }

    private function editLists($action, $contactID, array $contactsListIDs): array
    {
        $body = [Strings::C_CONTACTS_LISTS => []];
        foreach ($contactsListIDs as $list) {
            $body[Strings::C_CONTACTS_LISTS][] = [
                    Strings::C_LIST_ID => $list,
                    Strings::C_ACTION  => $action,
            ];
        }
        return $this->post(
                Resources::$ContactManagecontactslists,
                [Strings::U_ID => $contactID, Strings::BODY => $body]
        );
    }
}
