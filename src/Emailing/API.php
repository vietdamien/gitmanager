<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : API
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Emailing;

use Mailjet\Client;

abstract class API
{
    /**
     * @var Client the client that is used to execute Mailjet requests.
     */
    protected Client $client;

    /**
     * API constructor.
     *
     * @param string $publicKey  the public key of the API
     * @param string $privateKey the private key of the API
     */
    public function __construct(string $publicKey, string $privateKey)
    {
        $this->client = new Client($publicKey, $privateKey);
    }

    /**
     * Retrieves the needed information that corresponds to the given resources and the given attributes.
     *
     * @param array      $resources  the given resources
     * @param array|null $attributes the given attributes
     *
     * @return array the result as a JSON array
     */
    protected function getResults(array $resources, array $attributes = null): array
    {
        $response = $this->client->get($resources, $attributes ?: []);
        return $response->getData();
    }

    /**
     * Puts the data that corresponds to the given resources and the given attributes.
     *
     * @param array $resources the given resources
     * @param array $array     the given attributes
     *
     * @return array the result as a JSON array
     */
    protected function put(array $resources, array $array): array
    {
        $response = $this->client->put($resources, $array);
        return $response->getData();
    }

    /**
     * Sends data that correspond to the given resources and the given attributes to the MailJet server.
     *
     * @param array      $resources  the given resources
     * @param array|null $attributes the given attributes
     *
     * @return array the result as a JSON array
     */
    protected function post(array $resources, ?array $attributes): array
    {
        $response = $this->client->post($resources, $attributes);
        return $response->getData();
    }
}
