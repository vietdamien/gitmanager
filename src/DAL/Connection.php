<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Connection
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\DAL;

use App\Utilities\Configuration\Database;
use Exception;
use PDO;
use PDOStatement;

class Connection extends PDO
{
    /**
     * The index of the parameter value to bind.
     */
    private const PARAMETER_VALUE_INDEX = 0;
    /**
     * The index of the parameter value type to bind.
     */
    private const PARAMETER_TYPE_INDEX = 1;
    /**
     * @var PDOStatement The SQL statement.
     */
    private PDOStatement $statement;

    /**
     * Connection constructor.
     */
    public function __construct()
    {
        parent::__construct(Database::DSN, Database::USERNAME, Database::PASSWORD);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Counts the number of rows affected by a DELETE, INSERT, or UPDATE query.
     *
     * @return int the number of rows affected by a DELETE, INSERT, or UPDATE query
     */
    public function updatedRowCount(): int
    {
        return $this->statement->rowCount();
    }

    /**
     * Executes the given SQL query with the given array of parameters.
     *
     * @param       $query      string the given SQL query
     * @param array $parameters the given array of parameters
     *
     * @return bool true if the query was executed, false otherwise
     * @throws Exception if the query broke a constraint or is impossible
     */
    public function execute(string $query, array $parameters = []): bool
    {
        $this->statement = parent::prepare($query);
        foreach ($parameters as $name => $value) {
            $this->statement->bindValue($name, $value[self::PARAMETER_VALUE_INDEX], $value[self::PARAMETER_TYPE_INDEX]);
        }
        return $this->statement->execute();
    }

    /**
     * Gets the results of a SQL query.
     *
     * @return array the results of a SQL query
     * @throws Exception if there was an error fetching the results
     */
    public function getResults(): array
    {
        return $this->statement->fetchall();
    }

    /**
     * Checks whether the deletion was successful.
     *
     * @return bool true if the deletion was successful, false otherwise
     */
    public function rowsUpdated(): bool
    {
        return $this->updatedRowCount() != 0;
    }
}
