<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : ProjectGateway
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\DAL\Gateways;

use App\DAL\QueryBuilders\Delete;
use App\DAL\QueryBuilders\Insert;
use App\DAL\QueryBuilders\Select;
use App\Entities\Project;
use Exception;
use PDO;

class ProjectGateway extends Gateway
{
    /**
     * Deletes the project that has the given project ID.
     *
     * @param string $projectId the given project ID
     * @param string $userId    the concerned user ID
     *
     * @return bool true if the deletion was successful, false otherwise
     * @throws Exception if there was a problem deleting the project
     */
    public function remove(string $projectId, string $userId): bool
    {
        $deleteSQL = (new Delete())->addTable('project_subscriptions')
                                   ->andWhere('`project_id` = :projectId')->andWhere('`user_id` = :userId');
        $countSQL  = (new Select())->addColumn('COUNT(1)', 'count')
                                   ->addTable('project_subscriptions')
                                   ->andWhere('`project_id` = :projectId');

        $success = $this->connection->execute(
                $deleteSQL->get(),
                [
                        ':projectId' => [$projectId, PDO::PARAM_STR],
                        ':userId'    => [$userId, PDO::PARAM_STR],
                ]
        );
        $success &= $this->connection->execute($countSQL->get(), [':projectId' => [$projectId, PDO::PARAM_STR]]);

        if ($this->connection->getResults()[0]['count'] == 0) {
            $deleteSQL = (new Delete())->addTable('projects')
                                       ->andWhere('`id` = :projectId');

            $success &= $this->connection->execute($deleteSQL->get(), [':projectId' => [$projectId, PDO::PARAM_STR]]);
        }

        return $success;
    }

    /**
     * Finds all the projects that match the given link (if given).
     *
     * @param string|null $link the given link
     *
     * @return Project[] all the matching projects
     * @throws Exception if there was a problem retrieving the projects
     */
    public function find(string $link = null): array
    {
        $query = (new Select())->addTable('projects')
                               ->addOrder('id', 'DESC');
        $data  = [];

        if (!empty($link)) {
            $query->andWhere('`link` = :link');
            $data = [':link' => [$link, PDO::PARAM_STR]];
        }

        $this->connection->execute($query->get(), $data);
        return $this->factory->create(Project::class, $this->connection->getResults());
    }

    /**
     * Finds all the projects that match the given user ID.
     *
     * @param string|null $userId the given user ID
     *
     * @return Project[] all the projects
     * @throws Exception if there was a problem retrieving the projects
     */
    public function findBySub(?string $userId): array
    {
        $subsSql = (new Select())->addColumn('project_id')
                                 ->addTable('project_subscriptions')
                                 ->andWhere('`user_id` = :userId')
                                 ->addOrder('id', 'DESC');
        $query   = (new Select())->addTable('projects')
                                 ->andWhere("`id` IN ({$subsSql->get()})");

        $this->connection->execute($query->get(), [':userId' => [$userId, PDO::PARAM_STR]]);
        return $this->factory->create(Project::class, $this->connection->getResults());
    }

    /**
     * Gets an array with the email addresses of all those who are subscribed to the project that has the given project
     * ID.
     *
     * @param string $projectId the given project ID
     *
     * @return array an array with the emails of all those who are subscribed
     * @throws Exception if there was a problem retrieving the email address
     */
    public function findSubscribersEmail(string $projectId): array
    {
        $subsSql = (new Select())->addColumn('user_id')
                                 ->addTable('project_subscriptions')
                                 ->andWhere('`project_id` = :projectId');
        $query   = (new Select())->addColumn('email')
                                 ->addTable('users')
                                 ->andWhere("`id` IN ({$subsSql->get()})");

        $this->connection->execute($query->get(), [':projectId' => [$projectId, PDO::PARAM_STR]]);
        return array_map(fn($entry) => $entry['email'], $this->connection->getResults());
    }

    /**
     * Inserts a new project.
     *
     * @param string  $userId the subscribed user's ID
     * @param Project $project
     *
     * @return bool true if the insertion was successful, false otherwise
     * @throws Exception if there was a problem inserting the new project
     */
    public function insert(string $userId, Project $project): bool
    {
        $query = (new Insert(ignore: true))->setTable('projects')
                               ->addValue('name', ':name')
                               ->addValue('link', ':link');
        $this->connection->execute(
                $query->get(),
                [
                        ':name' => [$project->getName(), PDO::PARAM_STR],
                        ':link' => [$project->getLink(), PDO::PARAM_STR],
                ]
        );

        $projectId = $this->connection->lastInsertId();
        if ($this->connection->updatedRowCount() == 0) {
            $query = (new Select())->addColumn('id')->addTable('projects')->andWhere('`link` = :link');

            $this->connection->execute($query->get(), [':link' => [$project->getLink(), PDO::PARAM_STR]]);
            $projectId = $this->connection->getResults()[0]['id'];
        }

        $query = (new Insert(ignore: true))->setTable('project_subscriptions')
                                           ->addValue('user_id', ':userId')
                                           ->addValue('project_id', ':projectId');
        return $this->connection->execute(
                $query->get(),
                [
                        ':userId'    => [$userId, PDO::PARAM_STR],
                        ':projectId' => [$projectId, PDO::PARAM_STR],
                ]
        );
    }
}
