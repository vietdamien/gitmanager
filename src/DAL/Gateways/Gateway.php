<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Gateway
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\DAL\Gateways;

use App\DAL\Connection;
use App\DAL\QueryBuilders\Select;
use App\Model\ArrayFactory;
use Exception;

abstract class Gateway
{
    /**
     * @var Connection The instance of Connection, used as a bridge to the MySQL database.
     */
    protected Connection $connection;

    /**
     * @var ArrayFactory The factory that is used to transform database results into an array of entities.
     */
    protected ArrayFactory $factory;

    /**
     * Gateway constructor.
     */
    public function __construct()
    {
        $this->connection = new Connection();
        $this->factory    = new ArrayFactory();
    }

    /**
     * Gets the instance of Connection.
     *
     * @return Connection the instance of Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * Resets the auto-increment for all the tables.
     *
     * @return bool true if the reset was successful, false otherwise
     * @throws Exception if there was a problem resetting the auto-increment
     */
    public function resetAutoIncrement(): bool
    {
        $sql = '';

        foreach ($this->getTables() as $table) {
            $sql .= "ALTER TABLE $table AUTO_INCREMENT = 1;";
        }

        return $this->connection->execute($sql);
    }

    /**
     * Gets the list of all the tables in the database.* Gets the list of all the tables in the database.
     *
     * @return array the list of all the tables in the database
     * @throws Exception if there was a problem retrieving the
     */
    private function getTables(): array
    {
        $this->connection->execute(
                (new Select())->addColumn('table_name', 'tables')
                              ->addTable('`information_schema`.`tables`')
                              ->andWhere('`table_schema` = DATABASE()')
                              ->get()
        );

        return array_map(fn($arr) => $arr[0], $this->connection->getResults());
    }
}
