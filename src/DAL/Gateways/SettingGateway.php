<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : SettingGateway
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\DAL\Gateways;

use App\DAL\QueryBuilders\Select;
use App\DAL\QueryBuilders\Update;
use App\Entities\Setting;
use Exception;
use PDO;

class SettingGateway extends Gateway
{
    /**
     * Finds all the settings if no key is specified.
     *
     * @param string|null $name the key of the setting
     *
     * @return Setting[] all the settings
     * @throws Exception if there was a problem retrieving the settings
     */
    public function find(string $name = null): array
    {
        $query = (new Select())->addTable('settings')
                               ->addOrder('name');
        $data  = [];

        if (!empty($name)) {
            $query->andWhere('`name` = :name');
            $data[':name'] = [$name, PDO::PARAM_STR];
        }

        $this->connection->execute($query->get(), $data);
        return $this->factory->create(Setting::class, $this->connection->getResults());
    }

    /**
     * Updates one of this application's settings.
     *
     * @param Setting $setting the updated setting
     *
     * @return bool true if the update was successful, false otherwise
     * @throws Exception if there was a problem updating the setting
     */
    public function edit(Setting $setting): bool
    {
        $query = (new Update())->addTable('settings')
                               ->addValue('value', ':value')
                               ->andWhere('`name` = :name');
        $this->connection->execute(
                $query->get(),
                [
                        ':name'  => [$setting->getName(), PDO::PARAM_STR],
                        ':value' => [$setting->getValue(), PDO::PARAM_STR],
                ]
        );

        return $this->connection->rowsUpdated();
    }
}
