<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : UserGateway
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\DAL\Gateways;

use App\DAL\QueryBuilders\Select;
use App\Entities\User;
use Exception;
use PDO;

class UserGateway extends Gateway
{
    /**
     * Finds all the users if no email is specified.
     *
     * @param string|null $email the email of the User whose information is needed
     *
     * @return User[] all the users
     * @throws Exception if there was a problem retrieving the users
     */
    public function find(string $email = null): array
    {
        $query = (new Select())->addColumn('id')->addColumn('username')->addColumn('email')
                               ->addTable('users')
                               ->addOrder('username');
        $data  = [];

        if (!empty($email)) {
            $query->andWhere('`email` = :email');
            $data[':email'] = [$email, PDO::PARAM_STR];
        }

        $this->connection->execute($query->get(), $data);
        return $this->factory->create(User::class, $this->connection->getResults());
    }
}
