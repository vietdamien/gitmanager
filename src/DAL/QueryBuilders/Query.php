<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Query
 * Author      : Damien Nguyen
 * Date        : Saturday, July 10 2021 
 * ********************************************************************************************************************/

namespace App\DAL\QueryBuilders;

abstract class Query
{
    /**
     * Renders the full SQL query.
     *
     * @return string the full SQL query as a string
     */
    abstract public function get(): string;
}
