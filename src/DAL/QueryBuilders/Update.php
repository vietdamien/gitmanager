<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Update
 * Author      : Damien Nguyen
 * Date        : Saturday, July 10 2021 
 * ********************************************************************************************************************/

namespace App\DAL\QueryBuilders;

use JetBrains\PhpStorm\Pure;

class Update extends FilteredQuery
{
    /**
     * @var array The list of column-value pairs.
     */
    protected array $pairs;

    /**
     * Update constructor.
     */
    #[Pure] public function __construct()
    {
        parent::__construct();

        $this->pairs = [];
    }

    /**
     * Adds a new column-value pair the the list.
     *
     * @param string|null $column the column to set
     * @param string|null $value  the value to set
     *
     * @return $this this Insert
     */
    public function addValue(?string $column, ?string $value = null): self
    {
        $this->pairs["`$column`"] = $value ?? 'NULL';
        return $this;
    }

    /**
     * Renders the pairs list as a list of assignments.
     *
     * @return string the pairs list as a list of assignments
     */
    public function pairs(): string
    {
        array_walk($this->pairs, fn (&$value, $key) => $value = "$key = $value");

        return join(', ', $this->pairs);
    }

    /**
     * @inheritDoc
     */
    public function get(): string
    {
        return "UPDATE {$this->tables()} "
               . "SET {$this->pairs()} "
               . "WHERE {$this->where()}";
    }
}
