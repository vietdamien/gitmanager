<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Query
 * Author      : Damien Nguyen
 * Date        : Friday, July 09 2021 
 * ********************************************************************************************************************/

namespace App\DAL\QueryBuilders;

abstract class FilteredQuery extends Query
{
    /**
     * @var array The tables list in the FROM clause.
     */
    protected array $tables;

    /**
     * @var array The filters list in the WHERE clause.
     */
    protected array $where;

    /**
     * Query constructor.
     */
    public function __construct()
    {
        $this->tables = $this->where = [];
    }

    /**
     * Adds a table to the tables list. If only the table is specified, it is stored directly, otherwise an array with
     * the name of the table and its alias is stored.
     *
     * @param string|null $table the name of the table
     * @param string|null $alias the alias of the table
     * @param string|null $where the join clause
     *
     * @return $this this Query
     */
    public function addTable(?string $table, ?string $alias = '', ?string $where = ''): self
    {
        $marker = preg_match('/[`]/', $table) ? '' : '`';
        $this->tables[] = empty($alias) ? "$marker{$table}$marker" : ["$marker{$table}$marker", "`$alias`"];
        return $this->andWhere($where);
    }

    /**
     * Appends the given filter to the last filters list.
     *
     * @param string|null $filter the given filter
     *
     * @return $this this Query
     */
    public function andWhere(?string $filter): self
    {
        if (!empty($filter)) {
            $this->where[array_key_last($this->where)][] = $filter;
        }

        return $this;
    }

    /**
     * Appends a new array that contains the given filter to the filters list.
     *
     * @param string|null $filter the given filter
     *
     * @return $this this Query
     */
    public function orWhere(?string $filter): self
    {
        if (!empty($filter)) {
            $this->where[] = [$filter];
        }

        return $this;
    }

    /**
     * Renders the tables list in the FROM clause.
     *
     * @return string the tables list as a string
     */
    public function tables(): string
    {
        $parts = array_map(
                fn($part) => is_array($part) ? join(' ', $part) : $part,
                $this->tables
        );

        return join(', ', $parts);
    }

    /**
     * Renders the filters list in the WHERE clause.
     *
     * @return string the filters list as a string
     */
    public function where(): string
    {
        if (empty($this->where)) {
            return 1;
        }

        $andParts = array_map(
                fn($part) => join(' AND ', $part),
                $this->where
        );

        return (($uniquePart = count($this->where) === 1) ? '' : '(')
               . join(') OR (', $andParts)
               . ($uniquePart ? '' : ')');
    }
}
