<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Select
 * Author      : Damien Nguyen
 * Date        : Friday, July 09 2021 
 * ********************************************************************************************************************/

namespace App\DAL\QueryBuilders;

use JetBrains\PhpStorm\Pure;

class Select extends FilteredQuery
{
    /**
     * @var array The columns list in the SELECT clause.
     */
    protected array $columns;

    /**
     * @var array The columns list in the ORDER BY clause.
     */
    protected array $orders;

    /**
     * @var array The columns list in the GROUP BY clause.
     */
    protected array $groups;

    /**
     * Select constructor.
     */
    #[Pure] public function __construct()
    {
        parent::__construct();

        $this->columns = $this->orders = $this->groups = [];
    }

    /**
     * Adds a column to the SELECT columns list. If only the table is specified, it is stored directly, otherwise an
     * array with the name of the column and its alias is stored.
     *
     * @param string|null $name  the name of the column
     * @param string|null $alias the alias of the column
     *
     * @return $this this Select
     */
    public function addColumn(?string $name, ?string $alias = null): self
    {
        $marker          = preg_match('/[()]/', $name) ? '' : '`';
        $this->columns[] = empty($alias) ? "$marker{$name}$marker" : ["$marker{$name}$marker", "`$alias`"];
        return $this;
    }

    /**
     * Adds a column to the GROUP BY columns list.
     *
     * @param string|null $column the name of the column
     *
     * @return $this this Select
     */
    public function addGroup(?string $column): self
    {
        $this->groups[] = $column;
        return $this;
    }

    /**
     * Adds a column to the ORDER BY columns list.
     *
     * @param string|null $column    the name of the column
     * @param string|null $direction the keyword that indicates whether the rows are in ascending or descending order
     *
     * @return $this this Select
     */
    public function addOrder(?string $column, ?string $direction = null): self
    {
        $this->orders[] = "`$column`" . (empty($direction) || $direction === 'ASC' ? '' : " $direction");
        return $this;
    }

    /**
     * Renders the columns list in the SELECT clause.
     *
     * @return string the columns list as a string
     */
    public function columns(): string
    {
        if (empty($this->columns)) {
            return '*';
        }

        $parts = array_map(
                fn($part) => is_array($part) ? join(' AS ', $part) : $part,
                $this->columns
        );

        return join(', ', $parts);
    }

    /**
     * Renders the columns list in the GROUP BY clause.
     *
     * @return string|null the columns list as a string
     */
    public function groups(): ?string
    {
        return empty($this->groups) ? null : join(', ', $this->groups);
    }

    /**
     * Renders the columns list in the ORDER BY clause.
     *
     * @return string|null the columns list as a string
     */
    public function orders(): ?string
    {
        return empty($this->orders) ? null : join(', ', $this->orders);
    }

    /**
     * @inheritDoc
     */
    public function get(): string
    {
        return "SELECT {$this->columns()} "
               . "FROM {$this->tables()} "
               . "WHERE {$this->where()}"
               . (empty($groups = $this->groups()) ? '' : " GROUP BY $groups")
               . (empty($orders = $this->orders()) ? '' : " ORDER BY $orders");
    }
}
