<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Insert
 * Author      : Damien Nguyen
 * Date        : Saturday, July 10 2021 
 * ********************************************************************************************************************/

namespace App\DAL\QueryBuilders;

use JetBrains\PhpStorm\Pure;

class Insert extends Query
{
    /**
     * @var array The list of column-value pairs.
     */
    protected array $pairs;

    /**
     * @var string|null The name of the table where the value is inserted.
     */
    protected ?string $table;

    /**
     * @var bool|null The state which indicates whether to ignore duplicate insertions.
     */
    protected ?bool $ignore;

    /**
     * Insert constructor.
     *
     * @param string|null $table  the name of the table where the value is inserted
     * @param bool|null   $ignore the state which indicates whether to ignore duplicate insertions
     */
    public function __construct(?string $table = null, ?bool $ignore = false)
    {
        $this->pairs  = [];
        $this->table  = $table;
        $this->ignore = $ignore;
    }

    /**
     * Sets the name of the table.
     *
     * @param string|null $table the name of table
     *
     * @return $this this Insert
     */
    public function setTable(?string $table): self
    {
        $this->table = "`$table`";
        return $this;
    }

    /**
     * Adds a new column-value pair the the list.
     *
     * @param string|null $column the column to set
     * @param string|null $value  the value to set
     *
     * @return $this this Insert
     */
    public function addValue(?string $column, ?string $value = null): self
    {
        $this->pairs["`$column`"] = $value ?? 'NULL';
        return $this;
    }

    /**
     * Renders the columns list as a string.
     *
     * @return string the columns list as a string
     */
    public function columns(): string
    {
        return join(', ', array_keys($this->pairs));
    }

    /**
     * Renders the values list as a string.
     *
     * @return string the values list as a string
     */
    public function values(): string
    {
        return join(', ', $this->pairs);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function get(): string
    {
        return "INSERT" . ($this->ignore ? ' IGNORE ' : ' ') . "INTO $this->table ({$this->columns()}) "
               . "VALUES ({$this->values()})";
    }
}
