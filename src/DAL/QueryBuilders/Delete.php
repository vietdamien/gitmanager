<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Delete
 * Author      : Damien Nguyen
 * Date        : Saturday, July 10 2021 
 * ********************************************************************************************************************/

namespace App\DAL\QueryBuilders;

use JetBrains\PhpStorm\Pure;

class Delete extends FilteredQuery
{
    /**
     * Delete constructor.
     */
    #[Pure] public function __construct()
    {
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function get(): string
    {
        return "DELETE FROM {$this->tables()} "
               . "WHERE {$this->where()}";
    }
}
