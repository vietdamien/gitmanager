<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : View
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

class View
{
    /**
     * The extension for a Twig template file.
     */
    public const EXTENSION = '.html.twig';

    /**
     * View constructor.
     */
    private function __construct()
    {
    }

    /**
     * Redirects the user to the page that corresponds to the given action.
     *
     * @param string $baseURL this application's base URL
     * @param string $action  the given action
     */
    public static function redirect(string $baseURL, string $action = 'feed'): void
    {
        header('Location: ' . $baseURL . $action);
    }

    /**
     * Builds the template file name from the given action.
     *
     * @param string $action the given action
     *
     * @return string the built template file name
     */
    public static function buildTemplateName(string $action): string
    {
        return $action . self::EXTENSION;
    }
}
