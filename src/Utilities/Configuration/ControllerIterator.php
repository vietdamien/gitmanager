<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : ControllerIterator
 * Author      : Damien Nguyen
 * Date        : Thursday, July 08 2021 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

use App\Controllers\Controller;
use App\Controllers\ErrorController;
use RecursiveFilterIterator;

class ControllerIterator extends RecursiveFilterIterator
{
    private array $rejected = [
            Controller::class,
            ErrorController::class,
    ];
    
    /**
     * @inheritDoc
     */
    public function accept(): bool
    {
        $result = true;

        foreach ($this->rejected as $item) {
            $result &= Strings::buildControllerName($this->current()) !== $item;
        }

        return $result;
    }
}
