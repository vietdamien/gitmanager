<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Settings
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

class Settings
{
    public const MJ_NOTIFICATION_TEMPLATE = 'mj_notification_template';
    public const MJ_PASSWORD_TEMPLATE     = 'mj_password_template';
    public const MJ_PRIVATE_KEY           = 'mj_private_key';
    public const MJ_PUBLIC_KEY            = 'mj_public_key';
    public const MJ_SENDER_EMAIL          = 'mj_sender_email';
    public const MJ_SENDER_NAME           = 'mj_sender_name';
    public const MJ_WELCOME_TEMPLATE      = 'mj_welcome_template';

    public const KEYS = [
            self::MJ_NOTIFICATION_TEMPLATE => 'Mailjet notification template',
            self::MJ_PASSWORD_TEMPLATE     => 'Mailjet password template',
            self::MJ_PRIVATE_KEY           => 'Mailjet private key',
            self::MJ_PUBLIC_KEY            => 'Mailjet public key',
            self::MJ_SENDER_EMAIL          => 'Mailjet sender email address',
            self::MJ_SENDER_NAME           => 'Mailjet sender name',
            self::MJ_WELCOME_TEMPLATE      => 'Mailjet welcome template',
    ];

    /**
     * Settings constructor.
     */
    private function __construct()
    {
    }
}
