<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Strings
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

use SplFileInfo;

class Strings
{
    /**
     * An empty string.
     */
    public const EMPTY = '';

    /**
     * The Atom RSS feed extension.
     */
    public const RSS_EXTENSION = '.atom';

    /**
     * Strings constructor.
     */
    private function __construct()
    {
    }

    /**
     * Builds the fully qualified controller name from the given file information.
     *
     * @param SplFileInfo|null $fileInfo the given file information
     *
     * @return string the fully qualified controller name
     */
    public static function buildControllerName(SplFileInfo $fileInfo = null): string
    {
        return "App\Controllers\\{$fileInfo->getBasename('.php')}";
    }

    /**
     * Builds the setter function name from the given attribute.
     *
     * @param string|null $attribute the given attribute.
     *
     * @return string the setter function name
     */
    public static function buildSetterName(string $attribute = null): string
    {
        return !empty($attribute) ? 'set' . self::toPascalCase($attribute) : Strings::EMPTY;
    }

    /**
     * Builds this application's base URL.
     *
     * @return string this application's base URL
     */
    public static function buildBaseURL(): string
    {
        return "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$_SERVER['CONTEXT_PREFIX']}/GitManager/";
    }

    /**
     * Converts a string with word separators to a camelCase string.
     *
     * @param string|null $input     a string with word separators
     * @param string      $separator the word separator
     *
     * @return string the converted string
     */
    public static function toCamelCase(?string $input, string $separator = '_'): string
    {
        return lcfirst(self::toPascalCase($input, $separator));
    }

    /**
     * Converts a camelCase string to a snake_case string.
     *
     * @param string $input the camelCase string
     *
     * @return string the converted string
     */
    public static function toSnakeCase(string $input): string
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $input)), '_');
    }

    /**
     * Converts a string with word separators to a PascalCase string
     *
     * @param string|null $input     a string with word separators
     * @param string      $separator the word separator
     *
     * @return string the converted string
     */
    public static function toPascalCase(?string $input, string $separator = '_'): string
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }
}
