<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Route
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

use Closure;

class Route
{
    public const GET     = 'GET';
    public const POST    = 'POST';
    public const REQUEST = self::GET . '|' . self::POST;

    /**
     * @var string The method (GET, POST or GET|POST only).
     */
    private string $method;
    /**
     * @var string The URL suffix.
     */
    private string $pattern;
    /**
     * @var Closure The function or method to launch.
     */
    private Closure $closure;

    /**
     * Route constructor.
     *
     * @param string  $method  the method (GET, POST or GET|POST only)
     * @param string  $pattern the URL suffix
     * @param Closure $closure the function or method to launch
     */
    public function __construct(string $method, string $pattern, Closure $closure)
    {
        $this->method  = $method;
        $this->pattern = $pattern;
        $this->closure = $closure;
    }

    /**
     * Gets the method (GET, POST or GET|POST only).
     *
     * @return string the method (GET, POST or GET|POST only)
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Sets the URL suffix.
     *
     * @return string the URL suffix
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * Gets the function or method to launch.
     *
     * @return Closure the function or method to launch
     */
    public function getClosure(): Closure
    {
        return $this->closure;
    }
}
