<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Database
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

class Database
{
    /**
     * The data source name.
     */
    public const DSN = 'mysql:host=localhost;dbname=git_manager;charset=utf8mb4';
    /**
     * The MySQL username.
     */
    public const USERNAME = 'git_manager';
    /**
     * The MySQL password.
     */
    public const PASSWORD = 'G1TmAnAgEr_aCc0uNt*!';

    /**
     * The duplication error code.
     */
    public const DUPLICATION = 23000;

    /**
     * Database constructor.
     */
    private function __construct()
    {
    }
}
