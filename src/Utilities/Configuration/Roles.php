<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Roles
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Configuration;

use Delight\Auth\Role;

class Roles
{
    /**
     * The administrator role string.
     */
    public const ADMINISTRATOR = 'Administrator';
    /**
     * The member role string.
     */
    public const MEMBER = 'Member';
    /**
     * The visitor role string.
     */
    public const VISITOR = 'Visitor';
    /**
     * The parser role string.
     */
    public const PARSER = 'Parser';

    /**
     * The application roles mapped to the Auth framework roles.
     */
    public const ROLE_MAP = [
            self::ADMINISTRATOR => Role::ADMIN,
            self::MEMBER        => Role::SUBSCRIBER,
    ];

    /**
     * Roles constructor.
     */
    private function __construct()
    {
    }
}
