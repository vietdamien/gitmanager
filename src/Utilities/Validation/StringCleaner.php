<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : StringCleaner
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Validation;

use JetBrains\PhpStorm\Pure;

class StringCleaner extends Cleaner
{
    /**
     * @inheritDoc
     */
    public function isValid(mixed $value = null): bool
    {
        return !empty($value);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function clean(mixed $value = null): mixed
    {
        return filter_var($value, FILTER_SANITIZE_STRING);
    }
}
