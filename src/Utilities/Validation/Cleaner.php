<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Cleaner
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Validation;

abstract class Cleaner
{
    /**
     * Checks whether the given value is valid.
     *
     * @param mixed|null $value the given value
     *
     * @return bool true if it is, false otherwise
     */
    abstract public function isValid(mixed $value = null): bool;

    /**
     * Cleans the given value.
     *
     * @param mixed|null $value the given value
     *
     * @return mixed a clean value
     */
    abstract public function clean(mixed $value = null): mixed;
}
