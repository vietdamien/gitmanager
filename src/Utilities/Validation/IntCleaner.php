<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : IntCleaner
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Utilities\Validation;

use JetBrains\PhpStorm\Pure;

class IntCleaner extends Cleaner
{
    /**
     * @inheritDoc
     */
    #[Pure] public function isValid(mixed $value = null): bool
    {
        return isset($value) && filter_var($value, FILTER_VALIDATE_INT);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function clean(mixed $value = null): mixed
    {
        return (!isset($value) || $value == null || !$this->isValid($value))
                ? 0
                : filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }
}
