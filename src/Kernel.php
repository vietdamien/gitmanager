<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Kernel
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App;

use App\Controllers\ErrorController;
use App\Utilities\Configuration\ControllerIterator;
use App\Utilities\Configuration\Strings;
use Bramus\Router\Router;
use Error;
use Exception;
use RecursiveDirectoryIterator;

class Kernel
{
    /**
     * @var Router The router.
     */
    protected Router $router;

    /**
     * @var array All the controllers.
     */
    protected array $controllers;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        $this->controllers = $errors = [];
        $this->router      = new Router();

        try {
            $iterator = RecursiveDirectoryIterator::class;

            foreach (new ControllerIterator(new $iterator(ROOT . '/src/Controllers', $iterator::SKIP_DOTS)) as $file) {
                $controller          = Strings::buildControllerName($file);
                $this->controllers[] = new $controller($this->router);
            }

            $this->router->run();
        } catch (Error | Exception $e) {
            $errors[] = $e->getMessage();
        } finally {
            new ErrorController($this->router, $errors);
        }
    }
}
