<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : AdministratorController
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Controllers;

use App\Entities\Setting;
use App\Model\SettingModel;
use App\Model\UserModel;
use App\Utilities\Configuration\Roles;
use App\Utilities\Configuration\Route;
use App\Utilities\Configuration\Settings;
use App\Utilities\Configuration\View;
use App\Utilities\Validation\IntCleaner;
use Exception;
use JetBrains\PhpStorm\Pure;

class AdministratorController extends MemberController
{
    /**
     * @inheritDoc
     */
    #[Pure] public function getRoutes(): array
    {
        return [
                new Route(Route::GET, '/admin/settings', fn() => $this->settings()),
                new Route(Route::GET, '/admin/users', fn() => $this->users()),

                new Route(Route::POST, '/admin/settings', fn() => $this->editSettings()),
                new Route(Route::POST, '/admin/users', fn() => $this->removeUser()),
        ];
    }

    /**
     * Deletes a user.
     *
     * @throws Exception if there was a problem deleting the user
     */
    public function removeUser(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::ADMINISTRATOR]));

        (new UserModel($this->settings, $this->auth))->remove((new IntCleaner())->clean($_REQUEST['id']));
        $this->users();
    }

    /**
     * Shows the settings form.
     *
     * @throws Exception if there was a problem rendering the Twig template
     */
    public function settings(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::ADMINISTRATOR]));

        echo $this->twig->render(
                View::buildTemplateName(__FUNCTION__),
                [
                        'settings' => array_combine(
                                array_values(Settings::KEYS),
                                array_values($this->settings['db']),
                        ),
                ]
        );
    }

    /**
     * Updates all of this application's settings.
     *
     * @throws Exception if there was a problem updating the settings
     */
    public function editSettings(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::ADMINISTRATOR]));

        foreach ($_POST as $key => $value) {
            $this->settings['db'][$key]->setValue($value);
        }

        (new SettingModel($this->settings))->edit();
        $this->settings();
    }

    /**
     * Shows the user management page.
     *
     * @throws Exception if there was a problem rendering the Twig template
     */
    public function users(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::ADMINISTRATOR]));

        $userModel = new UserModel($this->settings, $this->auth);
        echo $this->twig->render(
                View::buildTemplateName(__FUNCTION__),
                ['users' => $userModel->find(),]
        );
    }
}
