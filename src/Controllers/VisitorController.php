<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : VisitorController
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Controllers;

use App\Model\UserModel;
use App\Utilities\Configuration\Route;
use App\Utilities\Configuration\Settings;
use App\Utilities\Configuration\View;
use App\Utilities\Validation\StringCleaner;
use Delight\Auth\InvalidPasswordException;
use Error;
use Exception;
use JetBrains\PhpStorm\Pure;

class VisitorController extends Controller
{
    /**
     * The default visitor action (called if no other actions are requested).
     */
    public const DEFAULT_ACTION = '/login';

    /**
     * @inheritDoc
     */
    #[Pure] public function getRoutes(): array
    {
        return [
                new Route(Route::GET, self::DEFAULT_ACTION, fn() => $this->login()),
                new Route(Route::GET, '/logout', fn() => $this->logout()),
                new Route(Route::GET, '/reset/password', fn() => $this->resetPassword()),
                new Route(Route::GET, '/register', fn() => $this->register()),

                new Route(Route::POST, self::DEFAULT_ACTION, fn() => $this->loginPost()),
                new Route(Route::POST, '/reset/password', fn() => $this->resetPasswordPost()),
                new Route(Route::POST, '/register', fn() => $this->registerPost()),
        ];
    }

    /**
     * Logs the user out if they do not fulfill the given predicate.
     *
     * @param bool $authorized the given predicate
     *
     * @throws Exception
     */
    public function authorize(bool $authorized)
    {
        if (!$authorized) {
            $this->logout();
            exit;
        }
    }

    /**
     * Displays the login form.
     *
     * @throws Exception if there was a problem rendering the Twig template
     */
    public function login(): void
    {
        echo $this->twig->render(View::buildTemplateName(__FUNCTION__));
    }

    /**
     * Logs the user in.
     *
     * @throws Exception if there was a problem with the authentication
     * @throws Error if there was a problem with the authentication
     */
    public function loginPost(): void
    {
        $cleaner = new StringCleaner();

        (new UserModel($this->settings, $this->auth))->login(
                $cleaner->clean($_REQUEST['username']),
                $cleaner->clean($_REQUEST['password'])
        );

        View::redirect($this->settings['app']['base_url']);
    }

    /**
     * Logs the user out.
     *
     * @throws Exception if there was a problem either rendering the Twig template or with the authentication
     * @throws Error if there was a problem either rendering the Twig template or with the authentication
     */
    public function logout(): void
    {
        (new UserModel($this->settings, $this->auth))->logout();
        $this->login();
    }

    /**
     * Displays the registration form.
     *
     * @throws Exception if there was a problem rendering the Twig template
     */
    public function register(): void
    {
        echo $this->twig->render(View::buildTemplateName(__FUNCTION__));
    }

    /**
     * Registers the user.
     *
     * @throws Exception if there was a problem either rendering the Twig template or with the authentication
     * @throws Error if there was a problem either rendering the Twig template or with the authentication
     */
    public function registerPost(): void
    {
        $cleaner = new StringCleaner();

        if ($cleaner->clean($_REQUEST['check']) !== ($password = $cleaner->clean($_REQUEST['password']))) {
            throw new InvalidPasswordException('Passwords do not match');
        }

        (new UserModel($this->settings, $this->auth))->register(
                $cleaner->clean($_REQUEST['username']),
                $cleaner->clean($_REQUEST['email']),
                $password
        );

        View::redirect($this->settings['app']['base_url']);
    }

    /**
     * Displays the password recovery form.
     *
     * @throws Exception if there was a problem either rendering the Twig template
     */
    public function resetPassword(): void
    {
        echo $this->twig->render(View::buildTemplateName(__FUNCTION__));
    }

    /**
     * Resets the user's password.
     *
     * @throws Exception if there was a problem resetting the password
     */
    public function resetPasswordPost(): void
    {
        (new UserModel($this->settings, $this->auth))->resetPassword((new StringCleaner())->clean($_REQUEST['email']));
        $this->login();
    }
}
