<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : MemberController
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Controllers;

use App\Entities\Project;
use App\Model\ProjectModel;
use App\Model\UserModel;
use App\Model\XMLParser;
use App\Utilities\Configuration\Roles;
use App\Utilities\Configuration\Route;
use App\Utilities\Configuration\Settings;
use App\Utilities\Configuration\Strings;
use App\Utilities\Configuration\View;
use App\Utilities\Validation\IntCleaner;
use App\Utilities\Validation\StringCleaner;
use Delight\Auth\InvalidPasswordException;
use Error;
use Exception;
use JetBrains\PhpStorm\Pure;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MemberController extends VisitorController
{
    public const DEFAULT_ACTION = '/feed';

    /**
     * @inheritDoc
     */
    #[Pure] public function getRoutes(): array
    {
        return [
                new Route(Route::GET, self::DEFAULT_ACTION, fn() => $this->feed()),
                new Route(Route::GET, '/add/project', fn() => $this->addProject()),
                new Route(Route::GET, '/edit/password', fn() => $this->editPassword()),

                new Route(Route::POST, self::DEFAULT_ACTION, fn() => $this->removeProject()),
                new Route(Route::POST, '/add/project', fn() => $this->addProjectPost()),
                new Route(Route::POST, '/edit/password', fn() => $this->editPasswordPost()),

                new Route(Route::REQUEST, '/', fn() => $this->feed()),
        ];
    }

    /**
     * Displays the new project form.
     *
     * @throws LoaderError if there was an error loading the Twig template
     * @throws RuntimeError if there was an unexpected error
     * @throws SyntaxError if there was a typo
     * @throws Exception
     */
    public function addProject(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::MEMBER]));

        echo $this->twig->render(View::buildTemplateName(__FUNCTION__));
    }

    /**
     * Inserts a new project.
     *
     * @throws Exception if there was a problem inserting the new project
     */
    public function addProjectPost(): void
    {
        $cleaner = new StringCleaner();
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::MEMBER]));

        $parser = new XMLParser(($link = $cleaner->clean($_REQUEST['link'])) . Strings::RSS_EXTENSION);
        $title  = explode(' ', $parser->parse()[0])[0];

        (new ProjectModel($this->settings))->add(
                $this->auth->getUserId(),
                (new Project())->setName($title)
                               ->setLink($link)
        );

        View::redirect($this->settings['app']['base_url']);
    }

    /**
     * Displays the password change form.
     *
     * @throws Exception if there was a problem rendering the Twig template
     */
    public function editPassword(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::MEMBER]));

        echo $this->twig->render(View::buildTemplateName(__FUNCTION__));
    }

    /**
     * Changes a user's password.
     *
     * @throws Exception if there was a problem changing the password
     */
    public function editPasswordPost(): void
    {
        $cleaner = new StringCleaner();
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::MEMBER]));

        if (!$this->auth->reconfirmPassword($cleaner->clean($_REQUEST['old']))) {
            throw new InvalidPasswordException('Old password is incorrect!');
        }

        if ($cleaner->clean($_REQUEST['check']) !== ($password = $cleaner->clean($_REQUEST['new']))) {
            throw new InvalidPasswordException('Passwords do not match!');
        }

        (new UserModel($this->settings, $this->auth))->editPassword($cleaner->clean($_REQUEST['email']), $password);

        $this->logout();
    }

    /**
     * Displays all the projects.
     *
     * @throws Exception if there was a problem retrieving the projects
     * @throws Error if there was a problem rendering the Twig template
     */
    public function feed(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::MEMBER]));

        $projects = (new ProjectModel($this->settings))->findBySub(($auth = $this->auth)->getUserId());
        echo $this->twig->render(
                View::buildTemplateName(__FUNCTION__),
                ['projects' => $projects,]
        );
    }

    /**
     * Removes a project.
     *
     * @throws Exception if there was a problem removing the project
     */
    public function removeProject(): void
    {
        $this->authorize($this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::MEMBER]));

        ($projectModel = new ProjectModel($this->settings))->remove(
                (new IntCleaner())->clean($_REQUEST['id']),
                $this->auth->id()
        );
        $this->feed();
    }
}
