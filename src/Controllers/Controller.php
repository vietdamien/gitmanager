<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Controller
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Controllers;

use App\DAL\Connection;
use App\DAL\Gateways\SettingGateway;
use App\Entities\Setting;
use App\Utilities\Configuration\Roles;
use App\Utilities\Configuration\Route;
use App\Utilities\Configuration\Settings;
use App\Utilities\Configuration\Strings;
use Bramus\Router\Router;
use Delight\Auth\Auth;
use Exception;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class Controller
{
    /**
     * @var array All this application's settings.
     */
    protected array $settings;
    /**
     * @var Auth The authentication utility.
     */
    protected Auth $auth;
    /**
     * @var Environment The Twig environment to be able to display templates.
     */
    protected Environment $twig;

    /**
     * Controller constructor.
     *
     * @param Router $router the router
     */
    public function __construct(Router $router)
    {
        $this->auth = new Auth(new Connection(), null, null, false);
        $this->twig = new Environment(new FilesystemLoader(ROOT . '/view'));

        $this->initRoutes($router);
        $this->initSettings();
        $this->initTwigGlobals();
    }

    /**
     * Gets an array with all the available routes.
     *
     * @return Route[] an array with all the available routes
     */
    abstract public function getRoutes(): array;

    /**
     * Initializes all this application's settings to the values stored in the database if they exist, and to the empty
     * string if they do not exist.
     */
    private function initSettings(): void
    {
        try {
            $this->settings['db'] = array_combine(
                    array_map(fn(Setting $x) => $x->getName(), $settings = (new SettingGateway())->find()),
                    $settings
            );
        } catch (Exception) {
            $this->settings['db'] = array_combine(
                    array_keys(Settings::KEYS),
                    array_map(fn($x) => Strings::EMPTY, Settings::KEYS)
            );
        } finally {
            $this->settings['app'] = [
                    'app_name' => APP_NAME,
                    'base_url' => Strings::buildBaseURL(),
            ];
        }
    }

    /**
     * Initializes all needed global variables for Twig.
     */
    private function initTwigGlobals(): void
    {
        foreach ($this->settings['app'] as $key => $value) {
            $this->twig->addGlobal($key, $value);
        }


        if ($this->auth->isLoggedIn()) {
            $this->twig->addGlobal('username', $this->auth->getUsername());
            $this->twig->addGlobal('isAdmin', $this->auth->hasAnyRole(Roles::ROLE_MAP[Roles::ADMINISTRATOR]));
        }
    }

    /**
     * Initializes all the routes that correspond to an actor.
     *
     * @param Router $router the router
     */
    private function initRoutes(Router $router): void
    {
        foreach ($this->getRoutes() as $route) {
            $router->match($route->getMethod(), $route->getPattern(), $route->getClosure());
        }
    }
}
