<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : WebHookController
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Controllers;

use App\Emailing\EmailAPI;
use App\Emailing\Strings;
use App\Model\ProjectModel;
use App\Utilities\Configuration\Route;
use App\Utilities\Configuration\Settings;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class WebHookController extends Controller
{
    /**
     * @inheritDoc
     */
    #[Pure] public function getRoutes(): array
    {
        return [
                new Route(Route::REQUEST, '/notify', fn() => $this->notify()),
        ];
    }

    /**
     * @throws Exception
     */
    public function notify()
    {
        $emailAPI     = new EmailAPI(
                $this->settings['db'][Settings::MJ_PUBLIC_KEY]->getValue(),
                $this->settings['db'][Settings::MJ_PRIVATE_KEY]->getValue()
        );
        $projectModel = new ProjectModel($this->settings);

        if ($json = json_decode(file_get_contents("php://input"), true)) {
            $array   = $projectModel->find($json['project']['web_url']);
            $project = reset($array);
            $emailAPI->send(
                    $projectModel->findSubscribersEmail($project->getId()),
                    "[{$project->getName()}] Update",
                    $this->settings['db'][Settings::MJ_NOTIFICATION_TEMPLATE]->getValue(),
                    [
                            Strings::C_EMAIL => $this->settings['db'][Settings::MJ_SENDER_EMAIL]->getValue(),
                            Strings::C_NAME  => $this->settings['db'][Settings::MJ_SENDER_NAME]->getValue(),
                    ],
                    $this->getVariables($json['commits']['0'])
            );
        }
    }

    #[Pure] #[ArrayShape([
            Strings::MESSAGE      => "mixed",
            Strings::URL          => "mixed",
            Strings::AUTHOR_NAME  => "mixed",
            Strings::AUTHOR_EMAIL => "mixed",
            Strings::ADDED        => "string",
            Strings::MODIFIED     => "string",
            Strings::REMOVED      => "string",
    ])] private function getVariables(
            array $commit
    ): array {
        return [
                Strings::MESSAGE      => $commit['message'],
                Strings::URL          => $commit['url'],
                Strings::AUTHOR_NAME  => $commit['author']['name'],
                Strings::AUTHOR_EMAIL => $commit['author']['email'],
                Strings::ADDED        => $this->renderFiles($commit['added']),
                Strings::MODIFIED     => $this->renderFiles($commit['modified']),
                Strings::REMOVED      => $this->renderFiles($commit['removed']),
        ];
    }

    #[Pure] private function renderFiles(array $files): string
    {
        return !empty($files) ? join("<br>", $files) : 'none';
    }
}
