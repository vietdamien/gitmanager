<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : ErrorController
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Controllers;

use App\Utilities\Configuration\Route;
use App\Utilities\Configuration\View;
use Bramus\Router\Router;
use Error;
use JetBrains\PhpStorm\Pure;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ErrorController extends Controller
{
    /**
     * ErrorController constructor.
     *
     * @param Router $router the router
     * @param array  $errors the error messages array
     */
    public function __construct(Router $router, private array $errors)
    {
        parent::__construct($router);
        if (!empty($errors)) {
            $this->error();
        }
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getRoutes(): array
    {
        return [
                new Route(Route::REQUEST, '/error', fn() => $this->error()),
        ];
    }

    /**
     * Displays the error page.
     */
    public function error(): void
    {
        try {
            echo $this->twig->render(View::buildTemplateName(__FUNCTION__), ['errors' => $this->errors]);
        } catch (Error | LoaderError | RuntimeError | SyntaxError) {
            // This should never happen.
            var_dump(__FUNCTION__);
        }
    }
}

