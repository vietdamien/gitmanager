<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : User
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Entities;

use JetBrains\PhpStorm\Pure;

class User extends Entity
{
    /**
     * User constructor.
     *
     * @param string|null $id       this User's ID
     * @param string|null $username this User's username
     * @param string|null $email    this User's email address
     */
    #[Pure] public function __construct(
            protected ?string $id = null,
            protected ?string $username = null,
            protected ?string $email = null
    ) {
        parent::__construct($id);
    }

    /**
     * Gets this User's username.
     *
     * @return string|null this User's username
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * Sets this User's username.
     *
     * @param string|null $username this User's username
     *
     * @return self this User
     */
    public function setUsername(?string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Gets this User's email address.
     *
     * @return string|null this User's email address
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Sets this User's email address.
     *
     * @param string|null $email this User's email address
     *
     * @return self this User
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }
}
