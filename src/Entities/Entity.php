<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Entity
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Entities;

use App\Utilities\Configuration\Strings;

abstract class Entity
{
    /**
     * Entity constructor.
     *
     * @param string|null $id this Entity's ID
     */
    public function __construct(protected ?string $id = null)
    {
    }

    /**
     * Gets this Entity's ID.
     *
     * @return string|null this Entity's ID
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Sets this Entity's ID.
     *
     * @param string|null $id this Entity's ID
     *
     * @return self this Entity
     */
    public function setId(?string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Binds the database results array to this Entity.
     *
     * @param array $data the database results array
     *
     * @return $this this Entity
     */
    public function bind(array $data): self
    {
        foreach ($data as $column => $value) {
            if (property_exists($this, $property = Strings::toCamelCase($column))) {
                $this->$property = $value;
            }
        }

        return $this;
    }
}
