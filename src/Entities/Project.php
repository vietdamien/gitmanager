<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Project
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Entities;

use JetBrains\PhpStorm\Pure;

class Project extends Entity
{
    /**
     * Project constructor.
     *
     * @param string|null $id   this Project's ID
     * @param string|null $name this Project's name
     * @param string|null $link this Project's link
     */
    #[Pure] public function __construct(
            protected ?string $id = null,
            protected ?string $name = null,
            protected ?string $link = null
    ) {
        parent::__construct($id);
    }

    /**
     * Gets this Project's name.
     *
     * @return string|null this Project's name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Sets this Project's name.
     *
     * @param string|null $name this Project's name
     *
     * @return self this Project
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Gets this Project's link.
     *
     * @return string|null this Project's link
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * Sets this Project's link.
     *
     * @param string|null $link this Project's link
     *
     * @return self this Project
     */
    public function setLink(?string $link): self
    {
        $this->link = $link;
        return $this;
    }
}
