<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Setting
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Entities;

use JetBrains\PhpStorm\Pure;

class Setting extends Entity
{
    /**
     * Setting constructor.
     *
     * @param string|null $id    this Setting's ID
     * @param string|null $name  this Setting's key
     * @param string|null $value this Setting's value
     */
    #[Pure] public function __construct(
            protected ?string $id = null,
            protected ?string $name = null,
            protected ?string $value = null
    ) {
        parent::__construct($id);
    }

    /**
     * Gets this Setting's key.
     *
     * @return string|null this Setting's key
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Sets this Setting's key.
     *
     * @param string|null $name this Setting's key
     *
     * @return self this Setting
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Gets this Setting's value.
     *
     * @return string|null this Setting's value
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * Sets this Setting's value.
     *
     * @param string|null $value this Setting's value
     *
     * @return self this Setting
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
