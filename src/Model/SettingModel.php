<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : SettingModel
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use App\DAL\Gateways\SettingGateway;
use App\Entities\Setting;
use Exception;

class SettingModel extends Model
{
    /**
     * SettingModel constructor.
     *
     * @param array $settings all this application's settings
     */
    public function __construct(array $settings)
    {
        parent::__construct($settings);
        $this->gateway = new SettingGateway();
    }

    /**
     * Finds all the settings if no key is specified.
     *
     * @param string|null $name the key of the setting
     *
     * @return array all the settings
     * @throws Exception if there was a problem retrieving the users
     */
    public function find(string $name = null): array
    {
        return $this->gateway->find($name);
    }

    /**
     * Updates all of this application's settings.
     *
     * @return bool true if the update was successful, false otherwise
     * @throws Exception if there was a problem updating the setting
     */
    public function edit(): bool
    {
        $updated = true;

        foreach (array_filter($this->settings['db'], fn($x) => $x instanceof Setting) as $setting) {
            $updated &= $this->gateway->edit($setting);
        }

        return $updated;
    }
}
