<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Factory
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use App\Utilities\Configuration\Strings;

class Factory
{
    /**
     * Creates an entity from the given attributes.
     *
     * @param string $class      the class name of the created Entity
     * @param array  $attributes the given attributes
     *
     * @return mixed an entity
     */
    public function create(string $class, array $attributes): mixed
    {
        return (new $class())->bind($attributes);
    }
}
