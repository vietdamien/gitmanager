<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : UserModel
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use App\DAL\Gateways\UserGateway;
use App\Emailing\ContactAPI;
use App\Emailing\EmailAPI;
use App\Emailing\Strings;
use App\Utilities\Configuration\Roles;
use App\Utilities\Configuration\Settings;
use Delight\Auth\Auth;
use Delight\Auth\EmailNotVerifiedException;
use Error;
use Exception;
use ReverseRegex\Generator\Scope;
use ReverseRegex\Lexer;
use ReverseRegex\Random\SimpleRandom;

class UserModel extends Model
{
    /**
     * UserModel constructor.
     *
     * @param array $settings all this application's settings
     * @param Auth  $auth     the authentication utility
     */
    public function __construct(array $settings, protected Auth $auth)
    {
        parent::__construct($settings);
        $this->gateway = new UserGateway();
    }

    /**
     * Sets the given password to the user who has the given email address.
     *
     * @param string $email    the given email address
     * @param string $password the given password
     *
     * @throws Exception if there was a problem resetting the password or the email was not registered
     */
    public function editPassword(string $email, string $password): void
    {
        $id = $this->auth->getUserId();

        if (!empty($email)) {
            if (count($potentialUsers = $this->gateway->find($email)) == 0) {
                throw new EmailNotVerifiedException('User was not found!');
            }

            $id = $potentialUsers[0]->getId();
        }

        $this->auth->admin()->changePasswordForUserById($id, $password);
    }

    /**
     * Deletes the user who has the given user ID.
     *
     * @param int $userId the given user ID
     *
     * @throws Exception if there was a problem deleting the user
     */
    public function remove(int $userId): void
    {
        $this->auth->admin()->deleteUserById($userId);
        $this->gateway->resetAutoIncrement();
    }

    /**
     * Finds all the users.
     *
     * @return array all the users
     * @throws Exception if there was a problem retrieving the users
     */
    public function find(): array
    {
        return $this->gateway->find();
    }

    /**
     * Logs the user in.
     *
     * @param string $username the user's username
     * @param string $password the user's password
     *
     * @throws Exception if there was a problem with the authentication
     * @throws Error if there was a problem with the authentication
     */
    public function login(string $username, string $password): void
    {
        $this->auth->loginWithUsername($username, $password);
    }

    /**
     * Logs the user out.
     *
     * @throws Exception if there was a problem with the authentication
     * @throws Error if there was a problem with the authentication
     */
    public function logout(): void
    {
        if ($this->auth->isLoggedIn()) {
            $this->auth->logOutEverywhere();
            $this->auth->destroySession();
        }
    }

    /**
     * Registers the user.
     *
     * @param string $username the user's username
     * @param string $email    the user's email address
     * @param string $password the user's password
     *
     * @throws Exception if there was a problem with the authentication
     * @throws Error if there was a problem with the authentication
     */
    public function register(string $username, string $email, string $password): void
    {
        $contactAPI = new ContactAPI(
                $this->settings['db'][Settings::MJ_PUBLIC_KEY]->getValue(),
                $this->settings['db'][Settings::MJ_PRIVATE_KEY]->getValue()
        );

        $this->auth->register($email, $password, $username);
        $this->auth->admin()->addRoleForUserByEmail($email, Roles::ROLE_MAP[Roles::MEMBER]);

        $new = ($contactAPI)->create($username, $email); // TODO: check on re-creation
        if ($id = reset($new)[Strings::U_ID]) {
            $contactAPI->updateFields($id, [Strings::NAME => $username]);
        }

        (new EmailAPI(
                $this->settings['db'][Settings::MJ_PUBLIC_KEY]->getValue(),
                $this->settings['db'][Settings::MJ_PRIVATE_KEY]->getValue()
        ))->send(
                [$email],
                'Registration complete',
                $this->settings['db'][Settings::MJ_WELCOME_TEMPLATE]->getValue(),
                [
                        Strings::C_EMAIL => $this->settings['db'][Settings::MJ_SENDER_EMAIL]->getValue(),
                        Strings::C_NAME  => $this->settings['db'][Settings::MJ_SENDER_NAME]->getValue(),
                ]
        );
    }

    /**
     * Resets the password for the user who has the given email address.
     *
     * @param string $email the given email address
     *
     * @throws Exception if there was a problem resetting the password or the email was not registered
     */
    public function resetPassword(string $email): void
    {
        if (count($potentialUsers = $this->gateway->find($email)) == 0) {
            throw new EmailNotVerifiedException('User was not found!');
        }

        $password = '';
        $parser   = new \ReverseRegex\Parser(new Lexer('[a-zA-Z0-9|*!?._&$]{24}'), new Scope(), new Scope());

        $parser->parse()->getResult()->generate($password, new SimpleRandom(rand(0, 1e10)));
        $this->auth->admin()->changePasswordForUserById(reset($potentialUsers)->getId(), $password);

        (new EmailAPI(
                $this->settings['db'][Settings::MJ_PUBLIC_KEY]->getValue(),
                $this->settings['db'][Settings::MJ_PRIVATE_KEY]->getValue()
        ))->send(
                [$email],
                'Password update',
                $this->settings['db'][Settings::MJ_PASSWORD_TEMPLATE]->getValue(),
                [
                        Strings::C_EMAIL => $this->settings['db'][Settings::MJ_SENDER_EMAIL]->getValue(),
                        Strings::C_NAME  => $this->settings['db'][Settings::MJ_SENDER_NAME]->getValue(),
                ],
                [Strings::PASSWORD => $password]
        );
    }
}
