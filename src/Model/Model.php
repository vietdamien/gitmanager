<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Model
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use App\DAL\Gateways\Gateway;

abstract class Model
{
    /**
     * @var Gateway The associated Gateway.
     */
    protected Gateway $gateway;

    /**
     * Model constructor.
     *
     * @param array $settings all this application's settings
     */
    public function __construct(protected array $settings)
    {
    }

    /**
     * Gets the Gateway associated to this Model.
     *
     * @return Gateway the Gateway associated to this Model
     */
    public function getGateway(): Gateway
    {
        return $this->gateway;
    }
}
