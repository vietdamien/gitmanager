<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : ArrayFactory
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use JetBrains\PhpStorm\Pure;

class ArrayFactory
{
    /**
     * @var Factory the factory that is used to create the right instances.
     */
    protected Factory $factory;

    /**
     * ArrayFactory constructor.
     */
    #[Pure] public function __construct()
    {
        $this->factory = new Factory();
    }

    /**
     * Creates an array of entities from the given results array.
     *
     * @param string $class   the class name of the created entities
     * @param array  $results the given results array
     *
     * @return array an array of entities
     */
    public function create(string $class, array $results): array
    {
        return array_map(fn($result) => $this->factory->create($class, $result), $results);
    }
}
