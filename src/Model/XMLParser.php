<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : XMLParser
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 01 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use Exception;

class XMLParser extends Parser
{
    /**
     * @inheritDoc
     *
     * @throws Exception if the file does not exist
     */
    public function parse(): array
    {
        $array = [];

        if (($nodes = $this->document->getElementsByTagName('title'))->length == 0) {
            throw new Exception('This document does not exist.');
        }

        foreach ($nodes as $key => $node) {
            $array[$key] = $node->nodeValue;
        }

        return $array;
    }
}
