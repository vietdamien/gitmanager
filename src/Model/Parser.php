<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : Parser
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 01 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use DOMDocument;

abstract class Parser
{
    /**
     * @var DOMDocument The document that represents the file to parse.
     */
    protected DOMDocument $document;

    /**
     * @var string The location of the file to parse.
     */
    protected string $file;

    /**
     * Parser constructor.
     *
     * @param string|null $file the location of the file to parse
     */
    public function __construct(string $file = null)
    {
        if (!empty($file)) {
            $this->setFile($file);
        }
    }

    /**
     * Gets the location of the file to parse.
     *
     * @return string the location of the file to parse
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * Sets the location of the file to parse.
     *
     * @param string $file the location of the file to parse
     */
    public function setFile(string $file)
    {
        $this->document = new DOMDocument();
        $this->document->load($this->file = $file);
    }

    /**
     * Parses the file, builds and returns an array.
     *
     * @return array an array
     */
    abstract public function parse(): array;
}
