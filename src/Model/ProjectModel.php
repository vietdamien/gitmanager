<?php
/* *********************************************************************************************************************
 * Project name: GitManager
 * File name   : ProjectModel
 * Author      : Damien Nguyen
 * Date        : Tuesday, March 31 2020 
 * ********************************************************************************************************************/

namespace App\Model;

use App\DAL\Gateways\ProjectGateway;
use App\Entities\Project;
use Exception;

class ProjectModel extends Model
{
    /**
     * ProjectModel constructor.
     *
     * @param array $settings all this application's settings
     */
    public function __construct(array $settings)
    {
        parent::__construct($settings);
        $this->gateway = new ProjectGateway();
    }

    /**
     * Deletes the project that has the given project ID.
     *
     * @param string $projectId the given project ID
     * @param string $userId    the concerned user ID
     *
     * @return bool true if the deletion was successful, false otherwise
     * @throws Exception if there was a problem deleting the project
     */
    public function remove(string $projectId, string $userId): bool
    {
        return $this->gateway->remove($projectId, $userId) && $this->gateway->resetAutoIncrement();
    }

    /**
     * Finds all the projects that match the given link (if given).
     *
     * @param string|null $link the given link
     *
     * @return array all the matching projects
     * @throws Exception if there was a problem retrieving the projects
     */
    public function find(string $link = null): array
    {
        return $this->gateway->find($link);
    }

    /**
     * Finds all the projects that match the given user ID.
     *
     * @param string|null $userId the given user ID
     *
     * @return array all the projects
     * @throws Exception if there was a problem retrieving the projects
     */
    public function findBySub(?string $userId): array
    {
        return $this->gateway->findBySub($userId);
    }

    /**
     * Gets an array with the email addresses of all those who are subscribed to the project that has the given project
     * ID.
     *
     * @param string $projectId the given project ID
     *
     * @return array an array with the emails of all those who are subscribed
     * @throws Exception if there was a problem retrieving the email address
     */
    public function findSubscribersEmail(string $projectId): array
    {
        return $this->gateway->findSubscribersEmail($projectId);
    }

    /**
     * Inserts a new project.
     *
     * @param string  $userId  the subscribed user's ID
     * @param Project $project the new project
     *
     * @return bool true if the insertion was successful, false otherwise
     * @throws Exception if there was a problem inserting the new project
     */
    public function add(string $userId, Project $project): bool
    {
        return $this->gateway->insert($userId, $project);
    }
}
